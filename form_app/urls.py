
from django.urls import path
from .views import createformthings,get_form_things,get_points,test



urlpatterns = [
    path('',createformthings,name='formview'),
    path('get_form/',get_form_things,name='get_form'),
    path('answers/',get_points,name='answers'),
    path('test/',test,name='test'),
]
