from django import forms
from .models import EditForm


class GoogleFormEditor(forms.ModelForm):
    class Meta:
        model = EditForm
        fields = '__all__'