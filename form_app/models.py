from django.db import models


class EditForm(models.Model):
    label = models.CharField(max_length=255)
    width = models.PositiveIntegerField()

    def __str__(self):
        return self.label