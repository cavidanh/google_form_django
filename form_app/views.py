from django.shortcuts import render
from .models import EditForm
from django.forms import modelformset_factory
from .forms import GoogleFormEditor
from django.utils.timezone import utc
import datetime

# Create your views here.

def createformthings(request):
    
    form = GoogleFormEditor()
    if request.method == 'POST':
        form = GoogleFormEditor(request.POST or None)
    context = {
        'form':form}

    return render(request, 'index.html',context)


def get_form_things(request):
    context = {}
    print(request.POST)
    if request.method == 'POST':
        label = request.POST.getlist('label')
       

        width = request.POST.get('width')
        context = {
            'name':request.POST.get('name'),
            'surname':request.POST.get('surname'),
            'title':request.POST.get('title'),
            'width':int(width),
            'liver':int((12-int(width))/2),
            'inputs': []
        }

        for i in range(len(label)):
            context['inputs'].append({
                'label':label[i],
            })

    return render(request,'get_answers.html',context)

def get_points(request):
    context = {}
    if request.method =='POST':
        answers = request.POST.getlist('answer')
        points = list(map(lambda x: int(x)*10, answers))
        

        overall = sum(points)
        context = {
            'points':points,
            'name':request.POST.get('name'),
            'overall':overall,
            'surname':request.POST.get('surname'),
            'time':datetime.datetime.utcnow().replace(tzinfo=utc)
        }
        
    return render (request,'answers.html',context)


def test(request):
    return render(request, 'index.html')